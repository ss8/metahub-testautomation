package tests;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

import pages.*;
import support.*;

public class ManageTeamsTest {
    private static ChromeDriver driver;

    @BeforeClass
    public static void setUP() throws IOException{
        driver = Starter.setUp("CHROME");
        LoginPage loginPage = new LoginPage(driver, "start");
        loginPage.typeUsername("estenico");
        loginPage.typePassword("Daitan=699");
        loginPage.clickSubmitButton();
    }
    
    @BeforeTest
    public void goToPeople() {
    	ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
    	manageUsersPage.clickPeopleButton();
    }

    @Test
    public void viewTeamTest() throws Exception {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        ManageTeamsPage manageTeamsPage = manageUsersPage.clickTeamsButton();
        ViewTeamPage viewTeamPage = manageTeamsPage.clickViewTeam();
        // if element is not enabled it fails the wait for element to be clickable so it throws TimeoutException
        try {
        	viewTeamPage.editTeamName("trytoedit");
        	throw new RuntimeException("Element should not be editable");
        } 
        catch (TimeoutException e) {}       
        manageTeamsPage = viewTeamPage.clickOkButton();
    }
    
    @Test
    public void addTeamTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        ManageTeamsPage manageTeamsPage = manageUsersPage.clickTeamsButton();
        CreateAndEditTeamPage createAndEditTeamPage = manageTeamsPage.clickCreateTeam();
        createAndEditTeamPage.editTeamName("testteam");
        createAndEditTeamPage.editTeamDescription("test description");
        createAndEditTeamPage.addUser("estenico");
        manageTeamsPage = createAndEditTeamPage.clickSaveButton();
        manageTeamsPage.inputSearchTeam("testteam");
        DeletePage deletePage = manageTeamsPage.clickDeleteTeam();
        manageTeamsPage = deletePage.clickDeleteButton(ManageTeamsPage.class);
        manageTeamsPage.deleteSearchTeam();
    }
    
    @Test
    public void editTeamTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        String teamname = "editteam"+ manageUsersPage.getTimeStamp();
        ManageTeamsPage manageTeamsPage = manageUsersPage.clickTeamsButton();
        CreateAndEditTeamPage createAndEditTeamPage = manageTeamsPage.clickCreateTeam();
        createAndEditTeamPage.editTeamName(teamname);
        createAndEditTeamPage.editTeamDescription("test description");
        createAndEditTeamPage.addUser("estenico");
        manageTeamsPage = createAndEditTeamPage.clickSaveButton();
        manageTeamsPage.inputSearchTeam(teamname);
        createAndEditTeamPage = manageTeamsPage.clickEditTeam();
        createAndEditTeamPage.editTeamName(teamname+"45");
        manageTeamsPage = createAndEditTeamPage.clickSaveButton();
        manageTeamsPage.inputSearchTeam(teamname+"45");
        DeletePage deletePage = manageTeamsPage.clickDeleteTeam();
        manageTeamsPage = deletePage.clickDeleteButton(ManageTeamsPage.class);
        manageTeamsPage.deleteSearchTeam();
    }

    @AfterClass
    public static void tearDown() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}
