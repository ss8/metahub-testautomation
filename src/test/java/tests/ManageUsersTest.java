package tests;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

import pages.*;
import support.*;

public class ManageUsersTest {
    private static ChromeDriver driver;
    String[] invalidEmails = {"testusergmail.com", "testuser@gmailcom", "test@user@gmail.com"};
    String[] invalidUsernames = {"@user", "#testuser"};

    @BeforeClass
    public static void setUP() throws IOException{
        driver = Starter.setUp("CHROME");
        LoginPage loginPage = new LoginPage(driver, "start");
        loginPage.typeUsername("estenico");
        loginPage.typePassword("Daitan=699");
        loginPage.clickSubmitButton();
    }
    
    @BeforeTest
    public void goToPeople() {
    	ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
    	manageUsersPage.clickPeopleButton();
    }

    @Test
    public void viewUserTest() throws Exception {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        ViewUserPage viewUserPage = manageUsersPage.clickViewUser();
        
        // if element is not enabled it fails the wait for element to be clickable so it throws TimeoutException
        try {
        	viewUserPage.editUserName("trytoedit");
        	throw new RuntimeException("Element should not be editable");
        } 
        catch (TimeoutException e) {}       
        manageUsersPage = viewUserPage.clickOkButton();
    }
    
    @Test
    public void addUserTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        CreateAndEditUserPage createAndEditUserPage = manageUsersPage.clickCreateUser();
        createAndEditUserPage.editUserName("testuser");
        createAndEditUserPage.editUserEmail("testuser@gmail.com");
        createAndEditUserPage.editUserFirstName("Firstname");
        createAndEditUserPage.editUserLastName("Lastname");
        createAndEditUserPage.addRole("Manage Users");
        createAndEditUserPage.editUserPassword("Password=234");
        manageUsersPage = createAndEditUserPage.clickSaveButton();
        manageUsersPage.inputSearchUser("testuser");
        DeletePage deletePage = manageUsersPage.clickDeleteUser();
        manageUsersPage = deletePage.clickDeleteButton(ManageUsersPage.class);
        manageUsersPage.deleteSearchUser();
    }
    
    @Test
    public void addUserInvalidEmailTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        CreateAndEditUserPage createAndEditUserPage = manageUsersPage.clickCreateUser();
        createAndEditUserPage.editUserName("testuser");
        createAndEditUserPage.editUserFirstName("Firstname");
        createAndEditUserPage.editUserLastName("Lastname");
        createAndEditUserPage.addRole("Manage Users");
        createAndEditUserPage.editUserPassword("Password=234");
        for (String email: invalidEmails) {           
        	createAndEditUserPage.editUserEmail(email);
        	createAndEditUserPage.emailErrorVisible();
        	try {
            	createAndEditUserPage.clickSaveButton();
            	throw new RuntimeException("Email error should pop and create screen remains");
            } 
            catch (TimeoutException e) {}      
        }
        manageUsersPage = createAndEditUserPage.clickCancelButton();
    }
    
    @Test
    public void addUserInvalidUsernameTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        CreateAndEditUserPage createAndEditUserPage = manageUsersPage.clickCreateUser();
        createAndEditUserPage.editUserFirstName("Firstname");
        createAndEditUserPage.editUserLastName("Lastname");
        createAndEditUserPage.addRole("Manage Users");
        createAndEditUserPage.editUserEmail("testuser@gmail.com");
        createAndEditUserPage.editUserPassword("Password=234");
        for (String username: invalidUsernames) {           
        	createAndEditUserPage.editUserName(username);
        	createAndEditUserPage.usernameErrorVisible();
        	try {
            	createAndEditUserPage.clickSaveButton();
            	throw new RuntimeException("Email error should pop and create screen remains");
            } 
            catch (TimeoutException e) {}      
        }
        manageUsersPage = createAndEditUserPage.clickCancelButton();
    }
    
    @Test
    public void editUserTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        String username = "edituser"+ manageUsersPage.getTimeStamp();
        CreateAndEditUserPage createAndEditUserPage = manageUsersPage.clickCreateUser();
        createAndEditUserPage.editUserName(username);
        createAndEditUserPage.editUserEmail("testuser@gmail.com");
        createAndEditUserPage.editUserFirstName("Firstname");
        createAndEditUserPage.editUserLastName("Lastname");
        createAndEditUserPage.addRole("Manage Users");
        createAndEditUserPage.editUserPassword("Password=234");
        manageUsersPage = createAndEditUserPage.clickSaveButton();
        manageUsersPage.inputSearchUser(username);
        createAndEditUserPage = manageUsersPage.clickEditUser();
        createAndEditUserPage.editUserName(username+"45");
        createAndEditUserPage.addRole("Manage Teams");
        manageUsersPage = createAndEditUserPage.clickSaveButton();
        manageUsersPage.inputSearchUser(username+"45");
        createAndEditUserPage = manageUsersPage.clickEditUser();
        createAndEditUserPage.removeRole("Manage Teams");
        manageUsersPage = createAndEditUserPage.clickSaveButton();
        DeletePage deletePage = manageUsersPage.clickDeleteUser();
        manageUsersPage = deletePage.clickDeleteButton(ManageUsersPage.class);
        manageUsersPage.deleteSearchUser();
    }
    
    @Test
    public void deactivateUserTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        manageUsersPage.inputSearchUser("ToDeactivateUser");
        EditUserPage EditUserPage = manageUsersPage.clickEditUser();
        EditUserPage.clickDeactivateUser();
        manageUsersPage = EditUserPage.clickSaveButton();
        manageUsersPage.clickManageAccountButton();
        LoginPage loginPage = manageUsersPage.clickLogoutButton();
        loginPage.typeUsername("ToDeactivateUser");
        loginPage.typePassword("Wordpass@567");
        loginPage.clickSubmitButton();
        loginPage.wrongPassErrorVisible();
        loginPage.typeUsername("estenico");
        loginPage.typePassword("Daitan=699");
        manageUsersPage = loginPage.clickSubmitButton();
        manageUsersPage.clickPeopleButton();
        manageUsersPage.inputSearchUser("ToDeactivateUser");
        EditUserPage = manageUsersPage.clickEditUser();
        EditUserPage.clickActivateUser();
        manageUsersPage = EditUserPage.clickSaveButton();
        manageUsersPage.deleteSearchUser();
    }

    @AfterClass
    public static void tearDown() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}
