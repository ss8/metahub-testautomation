package tests;

import java.lang.reflect.InvocationTargetException;
import org.openqa.selenium.TimeoutException;
import org.testng.annotations.Test;

import pages.*;

public class ManageRolesTest extends BaseTest{

    @Test
    public void viewRoleTest() throws Exception {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        ManageRolesPage manageRolesPage = manageUsersPage.clickRolesButton();
        ViewRolePage viewRolePage = manageRolesPage.clickViewRole("General Administration");
        // if element is not enabled it fails the wait for element to be clickable so it throws TimeoutException
        try {
        	viewRolePage.editRoleName("trytoedit");
        	throw new RuntimeException("Element should not be editable");
        } 
        catch (TimeoutException e) {}       
        manageRolesPage = viewRolePage.clickOkButton();
    }
    
    @Test
    public void addRoleTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        ManageRolesPage manageRolesPage = manageUsersPage.clickRolesButton();
        CreateAndEditRolePage createAndEditRolePage = manageRolesPage.clickCreateRole();
        createAndEditRolePage.editRoleName("testrole");
        createAndEditRolePage.editRoleDescription("test description");
        createAndEditRolePage.addPermission("Password and Access Policies");
        manageRolesPage = createAndEditRolePage.clickSaveButton();
        manageRolesPage.inputSearchRole("testrole");
        DeletePage deletePage = manageRolesPage.clickDeleteRole("testrole");
        manageRolesPage = deletePage.clickDeleteButton(ManageRolesPage.class);
        manageRolesPage.deleteSearchRole();
    }
    
    @Test
    public void editRoleTest() throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        String rolename = "editrole"+ manageUsersPage.getTimeStamp();
        ManageRolesPage manageRolesPage = manageUsersPage.clickRolesButton();
        CreateAndEditRolePage createAndEditRolePage = manageRolesPage.clickCreateRole();
        createAndEditRolePage.editRoleName(rolename);
        createAndEditRolePage.editRoleDescription("test description");
        createAndEditRolePage.addPermission("Password and Access Policies");
        manageRolesPage = createAndEditRolePage.clickSaveButton();
        manageRolesPage.inputSearchRole(rolename);
        createAndEditRolePage = manageRolesPage.clickEditRole(rolename);
        createAndEditRolePage.editRoleName(rolename+"45");
        manageRolesPage = createAndEditRolePage.clickSaveButton();
        manageRolesPage.inputSearchRole(rolename+"45");
        DeletePage deletePage = manageRolesPage.clickDeleteRole(rolename);
        manageRolesPage = deletePage.clickDeleteButton(ManageRolesPage.class);
        manageRolesPage.deleteSearchRole();
    }

}
