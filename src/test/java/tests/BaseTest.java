package tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import pages.*;
import support.*;

public class BaseTest {
    protected static ChromeDriver driver;
    protected static Properties properties;   

	@BeforeClass
    public static void setUpAndLogin() throws IOException{
        driver = Starter.setUp("CHROME");
        properties = new Properties();
        String currentPath = new java.io.File(".").getCanonicalPath();
    	FileInputStream fileInputStream = new FileInputStream(currentPath + "\\src\\test\\resources\\properties\\testdata.properties");
    	properties.load(fileInputStream);
    	LoginPage loginPage = new LoginPage(driver, "start");
        loginPage.typeUsername(properties.getProperty("mainUser"));
        loginPage.typePassword(properties.getProperty("mainUserPwd"));
        loginPage.clickSubmitButton();
    }
   
    @AfterMethod //AfterMethod annotation - This method executes after every test execution
    public void screenShot(ITestResult result){
	    //using ITestResult.FAILURE is equals to result.getStatus then it enter into if condition
	    if(ITestResult.FAILURE==result.getStatus()){
		    try{
			    // To create reference of TakesScreenshot
			    TakesScreenshot screenshot=(TakesScreenshot)driver;
			    // Call method to capture screenshot
			    File src=screenshot.getScreenshotAs(OutputType.FILE);
			    String currentPath = new java.io.File(".").getCanonicalPath();
			    // Copy files to specific location 
			    // result.getName() will return name of test case so that screenshot name will be same as test case name
			    FileUtils.copyFile(src, new File(currentPath + "\\src\\test\\resources\\screenshots\\"+result.getName()+".png"));
			    System.out.println("Successfully captured a screenshot");
			    }
		    catch (Exception e){
			    System.out.println("Exception while taking screenshot "+e.getMessage());
			} 
	    }
    }

    @AfterSuite
    public static void tearDown() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}
