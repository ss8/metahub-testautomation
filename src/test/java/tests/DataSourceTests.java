package tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import pages.*;
import support.*;

public class DataSourceTests {
	private static ChromeDriver driver;

    @BeforeClass
    public static void setUP() throws IOException{
        driver = Starter.setUp("CHROME");
        LoginPage loginPage = new LoginPage(driver, "start");
        loginPage.typeUsername("estenico");
        loginPage.typePassword("Daitan=699");
        loginPage.clickSubmitButton();
    }
    
    @AfterClass
    public static void tearDown() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}
