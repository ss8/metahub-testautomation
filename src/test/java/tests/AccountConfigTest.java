package tests;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import pages.*;
import support.*;

public class AccountConfigTest extends BaseTest{
    
    @Test
    public void verifyRolesPermissionsTest() throws Exception {
        ManageUsersPage manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.clickPeopleButton();
        ChromeDriver driver2 = Starter.setUp("CHROME");
        LoginPage loginPage2 = new LoginPage(driver2, "start");
        loginPage2.typeUsername("rolesuser");
        loginPage2.typePassword("Wordpass@567");
        ManageUsersPage manageUsersPage2 = loginPage2.clickSubmitButton();
        manageUsersPage2.clickPeopleButton();
        manageUsersPage.inputSearchUser("rolesuser");
        EditUserPage editUserPage = manageUsersPage.clickEditUser();
        editUserPage.addRole("Manage Dashboards/Reports");
        editUserPage.removeRole("General Administration");
        manageUsersPage = editUserPage.clickSaveButton();
        loginPage2 = manageUsersPage2.clickLogoutButton();
        loginPage2.typeUsername("rolesuser");
        loginPage2.typePassword("Wordpass@567");
        manageUsersPage2 = loginPage2.clickSubmitButton();
        manageUsersPage2.peopleButtonNotPresent();
        manageUsersPage2.analysisButtonNotPresent();
        manageUsersPage2.idossierButtonNotPresent();
        manageUsersPage2.dataSourceButtonNotPresent();
        ManageDashboardPage manageDashboardPage2 = manageUsersPage2.clickDashboardButton();
        ManageMonitoringPage manageMonitoringPage2 = manageDashboardPage2.clickMonitoringButton();
        editUserPage = manageUsersPage.clickEditUser();
        editUserPage.addRole("Manage Query Permissions");
        manageUsersPage = editUserPage.clickSaveButton();
        loginPage2 = manageUsersPage2.clickLogoutButton();
        loginPage2.typeUsername("rolesuser");
        loginPage2.typePassword("Wordpass@567");
        manageUsersPage2 = loginPage2.clickSubmitButton();
        manageUsersPage2.peopleButtonNotPresent();
        manageUsersPage2.idossierButtonNotPresent();
        manageUsersPage2.dataSourceButtonNotPresent();
        manageDashboardPage2 = manageUsersPage2.clickDashboardButton();
        manageMonitoringPage2 = manageDashboardPage2.clickMonitoringButton();
        ManageAnalysisPage manageAnalysisPage2 = manageMonitoringPage2.clickAnalysisButton();
        editUserPage = manageUsersPage.clickEditUser();
        editUserPage.addRole("Manage DataSource and Data Imports");
        manageUsersPage = editUserPage.clickSaveButton();
        loginPage2 = manageUsersPage2.clickLogoutButton();
        loginPage2.typeUsername("rolesuser");
        loginPage2.typePassword("Wordpass@567");
        manageUsersPage2 = loginPage2.clickSubmitButton();
        manageUsersPage2.peopleButtonNotPresent();
        manageUsersPage2.idossierButtonNotPresent();
        manageDashboardPage2 = manageUsersPage2.clickDashboardButton();
        manageMonitoringPage2 = manageDashboardPage2.clickMonitoringButton();
        manageAnalysisPage2 = manageMonitoringPage2.clickAnalysisButton();
        ManageDataSourcePage manageDataSourcePage2 = manageAnalysisPage2.clickDataSource();
        editUserPage = manageUsersPage.clickEditUser();
        editUserPage.removeRole("Manage Dashboards/Reports");
        editUserPage.removeRole("Manage Query Permissions");
        editUserPage.removeRole("Manage DataSource and Data Imports");
        editUserPage.addRole("General Administration");
        manageUsersPage = editUserPage.clickSaveButton();
        Thread.sleep(3000);
        driver2.quit();
    }
    
}
