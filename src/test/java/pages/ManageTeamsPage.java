package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ManageTeamsPage extends AbstractManagePage{

    final String NEW_TEAM_BTN = "//span[@class='bp3-button-text' and contains(text(),'New Team')]";
    final String SEARCH_TXT_FIELD = "//input[@class='bp3-input']";
    final String REFRESH_BTN = "//button[@class='bp3-button common-grid-refresh-button']";
    final String NAME_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String NAME_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String DESCRIPTION_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String DESCRIPTION_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String N_OF_MEMBERS_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String N_OF_MEMBERS_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String MEMBERS_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String MEMBERS_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String THREE_DOTS_FIRST_TEAM_BTN = "//span[@class='bp3-icon bp3-icon-more']";
    final String VIEW_FIRST_TEAM_BTN = "//*[@id=\"view\"]/div";
    final String EDIT_FIRST_TEAM_BTN = "//*[@id=\"edit\"]/div";
    final String DELETE_FIRST_TEAM_BTN = "//*[@id=\"delete\"]/div";

    public ManageTeamsPage(ChromeDriver driver) {
        super(driver);
    }

    public CreateAndEditTeamPage clickCreateTeam(){
    	clickOnElement(NEW_TEAM_BTN);
    	waitForPageToLoad();
    	return new CreateAndEditTeamPage(driver);
    }
    
    public void inputSearchTeam(String text){
    	deleteSearchTeam();
    	writeOnTextField(SEARCH_TXT_FIELD,text);
    	waitForPageToLoad();
    }
    
    public void clickRefreshButton(){
    	clickOnElement(REFRESH_BTN);
    }
    
    public void deleteSearchTeam(){
    	deleteTextField(SEARCH_TXT_FIELD);
    }

    public void inputSearchTeamName(String text){
    	writeOnTextFieldElementWithEqualXpaths(NAME_FILTER_TXT_FIELD,text,0);
    }
    
    public void clickFilterTeamName(){
    	clickOnElementWithEqualXpaths(NAME_FILTER_BTN,5);
    }
    
    public void inputSearchTeamDescription(String text){
    	writeOnTextFieldElementWithEqualXpaths(DESCRIPTION_FILTER_TXT_FIELD,text,1);
    }
    
    public void clickFilterTeamDescription(){
    	clickOnElementWithEqualXpaths(DESCRIPTION_FILTER_BTN,5);
    }
    
    public void inputSearchTeamNumberOfMembers(String text){
    	writeOnTextFieldElementWithEqualXpaths(N_OF_MEMBERS_FILTER_TXT_FIELD,text,2);
    }
    
    public void clickFilterTeamNumberOfMembers(){
    	clickOnElementWithEqualXpaths(N_OF_MEMBERS_FILTER_BTN,5);
    }
    
    public void inputSearchTeamMember(String text){
    	writeOnTextFieldElementWithEqualXpaths(MEMBERS_FILTER_TXT_FIELD,text,3);
    }
    
    public void clickFilterTeamMember(){
    	clickOnElementWithEqualXpaths(MEMBERS_FILTER_BTN,5);
    }
    
    public void clickThreeDotsFirstTeam(){
    	clickOnElementWithEqualXpaths(THREE_DOTS_FIRST_TEAM_BTN,0);
    }
    
    public ViewTeamPage clickViewTeam(){
    	clickThreeDotsFirstTeam();
    	hoverAndClickOnElement(VIEW_FIRST_TEAM_BTN);
    	waitForPageToLoad();
    	return new ViewTeamPage(driver);
    }
    
    public CreateAndEditTeamPage clickEditTeam(){
    	clickThreeDotsFirstTeam();
    	hoverAndClickOnElement(EDIT_FIRST_TEAM_BTN);
    	waitForPageToLoad();
    	return new CreateAndEditTeamPage(driver);
    }
    
    public DeletePage clickDeleteTeam(){
    	clickThreeDotsFirstTeam();
    	hoverAndClickOnElement(DELETE_FIRST_TEAM_BTN);
    	waitForPageToLoad();
    	return new DeletePage(driver);
    }
}
