package pages;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.chrome.ChromeDriver;

public class AboutPage extends AbstractPage{
			
    final String OK_BTN = "//button[@class='bp3-button bp3-intent-primary']";
    final String CLOSE_BTN = "//span[@class='bp3-icon bp3-icon-small-cross']";

    public AboutPage(ChromeDriver driver) {
        super(driver);
    }

    public <T extends AbstractManagePage> T clickOkButton(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
  	  clickOnElement(OK_BTN);
  	  waitForPageToLoad();
  	  return clazz.getDeclaredConstructor(ChromeDriver.class).newInstance(driver);
  	}
    
	public <T extends AbstractManagePage> T clickCloseButton(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
	  clickOnElement(CLOSE_BTN);
	  waitForPageToLoad();
	  return clazz.getDeclaredConstructor(ChromeDriver.class).newInstance(driver);
	}
}
