package pages;

import org.openqa.selenium.chrome.ChromeDriver;


public class ManageUsersPage extends AbstractManagePage{
			
    final String NEW_USER_BTN = "//span[@class='bp3-button-text' and contains(text(),'New User')]";
    final String SEARCH_TXT_FIELD = "//input[@class='bp3-input']";
    final String REFRESH_BTN = "//button[@class='bp3-button common-grid-refresh-button']";
    final String USERID_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String USERID_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String EMAIL_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String EMAIL_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String FIRST_NAME_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String FIRST_NAME_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String LAST_NAME_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String LAST_NAME_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String ROLES_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String ROLES_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String STATE_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String STATE_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String LAST_IP_FILTER_TXT_FIELD = "//input[@class='ag-floating-filter-input']";
    final String LAST_IP_FILTER_BTN = "//span[@class='ag-icon ag-icon-filter']";
    final String THREE_DOTS_FIRST_USER_BTN = "//span[@class='bp3-icon bp3-icon-more']";
    final String VIEW_FIRST_USER_BTN = "//*[@id=\"view\"]";
    final String EDIT_FIRST_USER_BTN = "//*[@id=\"edit\"]";
    final String DELETE_FIRST_USER_BTN = "//*[@id=\"delete\"]";

    public ManageUsersPage(ChromeDriver driver) {
        super(driver);
    }

    public CreateAndEditUserPage clickCreateUser(){
    	clickOnElement(NEW_USER_BTN);
    	waitForPageToLoad();
    	return new CreateAndEditUserPage(driver);
    }
    
    public void inputSearchUser(String text){
    	deleteSearchUser();
    	writeOnTextField(SEARCH_TXT_FIELD,text);
    	waitForPageToLoad();
    }
    
    public void deleteSearchUser(){
    	deleteTextField(SEARCH_TXT_FIELD);
    }
    
    public void clickRefreshButton(){
    	clickOnElement(REFRESH_BTN);
    }

    public void inputSearchUserId(String text){
    	writeOnTextFieldElementWithEqualXpaths(USERID_FILTER_TXT_FIELD,text,5);
    }
    
    public void clickFilterUserId(){
    	clickOnElementWithEqualXpaths(USERID_FILTER_BTN,5);
    }
    
    public void inputSearchUserEmail(String text){
    	writeOnTextFieldElementWithEqualXpaths(EMAIL_FILTER_TXT_FIELD,text,0);
    }
    
    public void clickFilterUserEmail(){
    	clickOnElementWithEqualXpaths(EMAIL_FILTER_BTN,0);
    }
    
    public void inputSearchFirstName(String text){
    	writeOnTextFieldElementWithEqualXpaths(FIRST_NAME_FILTER_TXT_FIELD,text,1);
    }
    
    public void clickFilterFirstName(){
    	clickOnElementWithEqualXpaths(FIRST_NAME_FILTER_BTN,6);
    }
    
    public void inputSearchUserLastName(String text){
    	writeOnTextFieldElementWithEqualXpaths(LAST_NAME_FILTER_TXT_FIELD,text,2);
    }
    
    public void clickFilterLastName(){
    	clickOnElementWithEqualXpaths(LAST_NAME_FILTER_BTN,6);
    }
    
    public void inputSearchUserRoles(String text){
    	writeOnTextFieldElementWithEqualXpaths(ROLES_FILTER_TXT_FIELD,text,3);
    }
    
    public void clickFilterRoles(){
    	clickOnElementWithEqualXpaths(ROLES_FILTER_BTN,6);
    }
    
    public void inputSearchUserState(String text){
    	writeOnTextFieldElementWithEqualXpaths(STATE_FILTER_TXT_FIELD,text,4);
    }
    
    public void clickFilterState(){
    	clickOnElementWithEqualXpaths(STATE_FILTER_BTN,6);
    }
    
    public void clickThreeDotsFirstUser(){
    	clickOnElementWithEqualXpaths(THREE_DOTS_FIRST_USER_BTN,0);
    }
    
    public ViewUserPage clickViewUser(){
    	clickThreeDotsFirstUser();
    	hoverAndClickOnElement(VIEW_FIRST_USER_BTN);
    	waitForPageToLoad();
    	return new ViewUserPage(driver);
    }
    
    public EditUserPage clickEditUser(){
    	clickThreeDotsFirstUser();
    	hoverAndClickOnElement(EDIT_FIRST_USER_BTN);
    	waitForPageToLoad();
    	return new EditUserPage(driver);
    }
    
    public DeletePage clickDeleteUser(){
    	clickThreeDotsFirstUser();
    	hoverAndClickOnElement(DELETE_FIRST_USER_BTN);
    	waitForPageToLoad();
    	return new DeletePage(driver);
    }
}
