package pages;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChangePasswordPage extends AbstractPage{
			
    final String CURRENT_PASSWORD = "//input[@name='password']";
    final String NEW_PASSWORD = "//input[@name='newpassword']";
    final String CONFIRM_NEW_PASSWORD = "//input[@name='confirmpassword']";
    final String CANCEL_BUTTON = "//button[@class='bp3-button bp3-large bp3-icon-submit']/../button[1]";
    final String SUBMIT_BUTTON = "//button[@class='bp3-button bp3-large bp3-icon-submit']/../button[2]";

    public ChangePasswordPage(ChromeDriver driver) {
        super(driver);
    }

    public void inputCurrentPassword(String text){
    	writeOnTextField(CURRENT_PASSWORD,text);
    }
    
    public void inputNewPassword(String text){
    	writeOnTextField(NEW_PASSWORD,text);
    }
    
    public void confirmNewPassword(String text){
    	writeOnTextField(CONFIRM_NEW_PASSWORD,text);
    }
    
    public <T extends AbstractManagePage> T clickCancelButton(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
    	clickOnElement(CANCEL_BUTTON);
    	return clazz.getDeclaredConstructor(ChromeDriver.class).newInstance(driver);
    }
    
    public LoginPage clickSubmitButton(){
    	clickOnElement(SUBMIT_BUTTON);
    	return new LoginPage(driver);
    }
}
