package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class AbstractManagePage extends AbstractPage{
	final String PEOPLE_BTN = "//*[@id=\"people\"]";
	final String ANALYSIS_BTN = "//*[@class='bp3-button-text' and contains(text(),'Analysis')]";
	final String MONITORING_BTN = "//*[@class='bp3-button-text' and contains(text(),'Monitoring')]";
	final String DASHBOARD_BTN = "//*[@class='bp3-button-text' and contains(text(),'Dashboard')]";
	final String IDOSSIERS_BTN = "//*[@class='bp3-button-text' and contains(text(),'iDossiers')]";
	final String DATA_SOURCE_BTN = "//*[@class='bp3-button-text' and contains(text(),'Data Source')]";
	final String MANAGE_ACCOUNT_BTN = "//span[@class='bp3-icon bp3-icon-user']";
	final String USERS_BTN = "//*[@id=\"users\"]/span";
	final String TEAMS_BTN = "//*[@id=\"teams\"]/span";
	final String ROLES_BTN = "//*[@id=\"roles\"]/span";
	final String LOGOUT_BTN = "//span[@class='bp3-icon bp3-icon-new-link']";
	final String CHANGE_PASS_BTN = "//span[@class='bp3-icon bp3-icon-key']";
	final String HELP_BTN = "//span[@class='bp3-icon bp3-icon-help']";
	final String ABOUT_BTN = "//span[@class='bp3-icon bp3-icon-lightbulb']";

    public AbstractManagePage (ChromeDriver driver) {
    	super(driver);
    }
    
    public ManageUsersPage clickPeopleButton(){
    	clickOnElement(PEOPLE_BTN);
    	waitForPageToLoad();
    	return new ManageUsersPage(driver);
    }
    
    public ManageDataSourcePage clickDataSource(){
    	clickOnElement(DATA_SOURCE_BTN);
    	waitForPageToLoad();
    	return new ManageDataSourcePage(driver);
    }
    
    public ManageAnalysisPage clickAnalysisButton(){
    	clickOnElement(ANALYSIS_BTN);
    	waitForPageToLoad();
    	return new ManageAnalysisPage(driver);
    }
    
    public ManageMonitoringPage clickMonitoringButton(){
    	clickOnElement(MONITORING_BTN);
    	waitForPageToLoad();
    	return new ManageMonitoringPage(driver);
    }
    
    public ManageDashboardPage clickDashboardButton(){
    	clickOnElement(DASHBOARD_BTN);
    	waitForPageToLoad();
    	return new ManageDashboardPage(driver);
    }
    
    public ManageIdossiersPage clickIdossiersButton(){
    	clickOnElement(IDOSSIERS_BTN);
    	waitForPageToLoad();
    	return new ManageIdossiersPage(driver);
    }
    
    public void peopleButtonNotPresent() {
    	waitForInvisibility(PEOPLE_BTN);
    }
    
    public void analysisButtonNotPresent() {
    	waitForInvisibility(ANALYSIS_BTN);
    }
    
    public void monitoringButtonNotPresent() {
    	waitForInvisibility(MONITORING_BTN);
    }
    
    public void dashboardButtonNotPresent() {
    	waitForInvisibility(DASHBOARD_BTN);
    }
    
    public void idossierButtonNotPresent() {
    	waitForInvisibility(IDOSSIERS_BTN);
    }
    
    public void dataSourceButtonNotPresent() {
    	waitForInvisibility(DATA_SOURCE_BTN);
    }
    
    public void clickManageAccountButton(){
    	clickOnElement(MANAGE_ACCOUNT_BTN);
    }
    
    public ManageUsersPage clickUsersButton(){
    	clickOnElement(USERS_BTN);
    	waitForPageToLoad();
    	return new ManageUsersPage(driver);
    }
    
    public ManageTeamsPage clickTeamsButton(){
    	clickOnElement(TEAMS_BTN);
    	waitForPageToLoad();
    	return new ManageTeamsPage(driver);
    }
    
    public ManageRolesPage clickRolesButton(){
    	clickOnElement(ROLES_BTN);
    	waitForPageToLoad();
    	return new ManageRolesPage(driver);
    }
    
    public LoginPage clickLogoutButton(){
    	clickManageAccountButton();
    	clickOnElement(LOGOUT_BTN);
    	waitForPageToLoad();
    	return new LoginPage(driver);
    }
    
    public ChangePasswordPage clickChangePassButton(){
    	clickManageAccountButton();
    	hoverAndClickOnElement(CHANGE_PASS_BTN);
    	waitForPageToLoad();
    	return new ChangePasswordPage(driver);
    }
    
    public HelpPage clickHelpButton(){
    	clickManageAccountButton();
    	hoverAndClickOnElement(HELP_BTN);
    	waitForPageToLoad();
    	return new HelpPage(driver);
    }
    
    public AboutPage clickAboutButton(){
    	clickManageAccountButton();
    	hoverAndClickOnElement(ABOUT_BTN);
    	waitForPageToLoad();
    	return new AboutPage(driver);
    }

}
