package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateAndEditTeamPage extends AbstractPage{
			
    final String TEAM_NAME_TXT_FIELD = "//input[@data-test='input-name']";
    final String TEAM_DESCRIPTION_TXT_FIELD = "//textarea[@data-test='input-description']";
    final String ALL_USERS_TEXT_FIELD = "//input[@class='bp3-input-ghost bp3-multi-select-tag-input-input']";
    final String USER_SELECTOR = "//div[@class='bp3-text-overflow-ellipsis bp3-fill' and contains(text(),'%s')]";
    final String CANCEL_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Cancel')]";
    final String SAVE_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Save')]";
  

    public CreateAndEditTeamPage(ChromeDriver driver) {
        super(driver);
    }

    public void editTeamName(String teamname){
    	clearTextField(TEAM_NAME_TXT_FIELD);
    	writeOnTextField(TEAM_NAME_TXT_FIELD, teamname);
    }
    
    public void editTeamDescription(String description){
    	clearTextField(TEAM_NAME_TXT_FIELD);
    	writeOnTextField(TEAM_DESCRIPTION_TXT_FIELD, description);
    }
    
    public void addUser(String user){
    	String roleXpath = String.format(USER_SELECTOR, user);
    	clickOnElement(ALL_USERS_TEXT_FIELD);
    	hoverAndClickOnElement(roleXpath);
    	clickOnElement(TEAM_NAME_TXT_FIELD);
    }
    
    public ManageTeamsPage clickCancelButton(){
    	clickOnElement(CANCEL_BUTTON);
    	waitForInvisibility(CANCEL_BUTTON);
        waitForPageToLoad();
    	return new ManageTeamsPage(driver);
    }
    
    public ManageTeamsPage clickSaveButton(){
    	clickOnElement(SAVE_BUTTON);
    	waitForInvisibility(SAVE_BUTTON);
        waitForPageToLoad();
    	return new ManageTeamsPage(driver);
    }  
    
}
