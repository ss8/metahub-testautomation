package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class EditUserPage extends CreateAndEditUserPage{
	
	final String DEACTIVATE_BUTTON = "//span[contains(text(),'Deactivate')]";
	final String ACTIVATE_BUTTON = "//span[contains(text(),'Activate')]";	
    final String FORCE_PASSWORD_CHANGE_BUTTON = "//span[contains(text(),'Force Password Change')]";
			
    public EditUserPage(ChromeDriver driver) {
        super(driver);
    }
    
    public void clickDeactivateUser(){
    	clickOnElement(DEACTIVATE_BUTTON);
    	waitForInvisibility(DEACTIVATE_BUTTON);
    }
    
    public void clickActivateUser(){
    	clickOnElement(ACTIVATE_BUTTON);
    	waitForInvisibility(ACTIVATE_BUTTON);
    }
    
    public void clickForcePasswordChange(){
    	clickOnElement(FORCE_PASSWORD_CHANGE_BUTTON);
    }
}
