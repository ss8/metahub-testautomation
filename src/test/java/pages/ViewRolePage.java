package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class ViewRolePage extends AbstractPage{
	final String INPUT_NAME_TXT_FIELD = "//input[@data-test='input-name']";
    final String DESCRIPTION_TEXT_AREA = "//textarea[@data-test='input-description']";
    final String PERMISSIONS = "//span[@class='rct-title' and contains(text(),'%s')]";
    final String EXPAND_ALL_BUTTON = "//span[@class='bp3-icon bp3-icon-expand-all bp3-intent-primary']";
    final String COLAPSE_ALL_BUTTON = "//span[@class='bp3-icon bp3-icon-collapse-all bp3-intent-primary']";
    final String OK_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Ok')]";

    public ViewRolePage(ChromeDriver driver) {
        super(driver);
    }
    
    public void editRoleName(String roleName){
    	writeOnTextField(INPUT_NAME_TXT_FIELD, roleName);
    }

    public void clickExpandAllRoles(){
    	scrollToElement(EXPAND_ALL_BUTTON);
    	hoverToElement(EXPAND_ALL_BUTTON);
    	clickOnElement(EXPAND_ALL_BUTTON);
    }
    
    public void clickColapseAllRoles(){
    	scrollToElement(COLAPSE_ALL_BUTTON);
    	hoverToElement(COLAPSE_ALL_BUTTON);
    	clickOnElement(COLAPSE_ALL_BUTTON);
    }
    
    public ManageRolesPage clickOkButton(){
    	clickOnElement(OK_BUTTON);
    	waitForInvisibility(OK_BUTTON);
        waitForPageToLoad();
    	return new ManageRolesPage(driver);
    }
}
