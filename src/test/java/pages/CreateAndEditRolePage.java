package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateAndEditRolePage extends AbstractPage{
			
    final String INPUT_NAME_TXT_FIELD = "//input[@data-test='input-name']";
    final String DESCRIPTION_TEXT_AREA = "//textarea[@data-test='input-description']";
    final String PERMISSIONS = "//span[@class='rct-title' and contains(text(),'%s')]";
    final String EXPAND_ALL_BUTTON = "//span[@class='bp3-icon bp3-icon-expand-all bp3-intent-primary']";
    final String COLAPSE_ALL_BUTTON = "//span[@class='bp3-icon bp3-icon-collapse-all bp3-intent-primary']";
    final String CANCEL_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Cancel')]";
    final String SAVE_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Save')]";
  

    public CreateAndEditRolePage(ChromeDriver driver) {
        super(driver);
    }

    public void editRoleName(String roleName){
    	clearTextField(INPUT_NAME_TXT_FIELD);
    	writeOnTextField(INPUT_NAME_TXT_FIELD, roleName);
    }
    
    public void editRoleDescription(String description){
    	clearTextField(DESCRIPTION_TEXT_AREA);
    	writeOnTextField(DESCRIPTION_TEXT_AREA, description);
    }
    
    public void addPermission(String permission){
    	String permissionXpath = String.format(PERMISSIONS, permission);
    	scrollToElement(EXPAND_ALL_BUTTON);
    	hoverAndClickOnElement(EXPAND_ALL_BUTTON);
    	clickOnElement(permissionXpath);
    }
    
    public void clickExpandAllRoles(){
    	scrollToElement(EXPAND_ALL_BUTTON);
    	hoverToElement(EXPAND_ALL_BUTTON);
    	clickOnElement(EXPAND_ALL_BUTTON);
    }
    
    public void clickColapseAllRoles(){
    	scrollToElement(COLAPSE_ALL_BUTTON);
    	hoverToElement(COLAPSE_ALL_BUTTON);
    	clickOnElement(COLAPSE_ALL_BUTTON);
    }
    
    public ManageRolesPage clickCancelButton(){
    	clickOnElement(CANCEL_BUTTON);
    	waitForInvisibility(CANCEL_BUTTON);
        waitForPageToLoad();
    	return new ManageRolesPage(driver);
    }
    
    public ManageRolesPage clickSaveButton(){
    	clickOnElement(SAVE_BUTTON);
    	waitForInvisibility(SAVE_BUTTON);
        waitForPageToLoad();
    	return new ManageRolesPage(driver);
    }  
    
}
