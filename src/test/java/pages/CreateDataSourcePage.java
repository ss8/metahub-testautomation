package pages;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateDataSourcePage extends AbstractPage{
			
    final String DATA_SOURCE_NAME = "//input[@id='data-source-input-name']";
    final String DATA_SOURCE_DESCRIPTION = "//input[@placeholder='Enter a description']";
    final String EVENT_TYPE_DROPDOWN = "//span[@class='bp3-button-text' and contains(text(),'Select Type')]";
    final String EVENT_TYPE_OPTION = "//div[@class='bp3-text-overflow-ellipsis bp3-fill' and contains(text(),'%s')]";
    final String BROWSE_FILE_BTN = "//span[@class='bp3-button-text' and contains(text(),'Browse')]";
    final String TABLE_ROW_ELEMENT = "//div[contains(text(),'%s')][1]/../div";
    final String SAVE_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Save')]";

    public CreateDataSourcePage(ChromeDriver driver) {
        super(driver);
    }

    public void editSourceName(String sourceName){
    	clearTextField(DATA_SOURCE_NAME);
    	writeOnTextField(DATA_SOURCE_NAME, sourceName);
    }
    
    public void editSourceDescription(String sourceDesc){
    	clearTextField(DATA_SOURCE_DESCRIPTION);
    	writeOnTextField(DATA_SOURCE_DESCRIPTION, sourceDesc);
    }
    
    public void pickEventType(String eventType){
    	String roleXpath = String.format(EVENT_TYPE_OPTION, eventType);
    	clickOnElement(EVENT_TYPE_DROPDOWN);
    	hoverAndClickOnElement(roleXpath);
    }
    
    public void uploadFile(String filename) throws IOException, AWTException{
    	clickOnElement(BROWSE_FILE_BTN);
    	String currentPath = new java.io.File(".").getCanonicalPath();
    	String filePath = currentPath + "\\src\\test\\resources\\"+ filename;
    	inputFile(filePath);
    }
    
    public void clickTableElement(String fieldName, int columnNumber){
    	String elementXpath = String.format(TABLE_ROW_ELEMENT, fieldName);
    	elementXpath = elementXpath + "["+ columnNumber + "]";
    	clickOnElement(elementXpath);
    }
    
    public void doubleClickTableElement(String fieldName, int columnNumber){
    	String elementXpath = String.format(TABLE_ROW_ELEMENT, fieldName);
    	elementXpath = elementXpath + "["+ columnNumber + "]";
    	doubleClickOnElement(elementXpath);
    }
    
    public void selectEventDate(String fieldName){
    	clickTableElement(fieldName,1);
    	doubleClickTableElement(fieldName,6);
    	clickTableElement(fieldName,6);
    	String selectXpath = String.format(TABLE_ROW_ELEMENT, fieldName);
    	selectXpath = selectXpath + "[6]/div/select";
    	selectFromDropdown(selectXpath,"Event Date");
    }
    
    public ManageDataSourcePage clickSaveButton(){
    	clickOnElementWithEqualXpaths(SAVE_BUTTON,1);
        waitForPageToLoad();
    	return new ManageDataSourcePage(driver);
    }
}
