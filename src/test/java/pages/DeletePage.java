package pages;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.chrome.ChromeDriver;

public class DeletePage extends AbstractPage{
			
    final String CANCEL_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Cancel')]";
    final String DELETE_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Delete')]";

    public DeletePage(ChromeDriver driver) {
        super(driver);
    }
    
    public <T extends AbstractManagePage> T clickCancelButton(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
  	  clickOnElement(CANCEL_BUTTON);
  	  waitForInvisibility(CANCEL_BUTTON);
  	  waitForPageToLoad();
  	  return clazz.getDeclaredConstructor(ChromeDriver.class).newInstance(driver);
  	}
    
    public <T extends AbstractManagePage> T clickDeleteButton(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, SecurityException {
      clickOnElement(DELETE_BUTTON);
      waitForInvisibility(DELETE_BUTTON);
      waitForPageToLoad();
      return clazz.getDeclaredConstructor(ChromeDriver.class).newInstance(driver);
    }
 
}
