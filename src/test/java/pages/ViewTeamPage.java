package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class ViewTeamPage extends AbstractPage{
			
	final String TEAM_NAME_TXT_FIELD = "//input[@data-test='input-name']";
    final String TEAM_DESCRIPTION_TXT_FIELD = "//textarea[@data-test='input-description']";
    final String ALL_USERS_TEXT_FIELD = "//input[@class='bp3-input-ghost bp3-multi-select-tag-input-input']";
    final String USER_SELECTOR = "//div[@class='bp3-text-overflow-ellipsis bp3-fill' and contains(text(),'%s')]";
    final String OK_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Ok')]";

    public ViewTeamPage(ChromeDriver driver) {
        super(driver);
    }
    
    public void editTeamName(String teamname){
    	writeOnTextField(TEAM_NAME_TXT_FIELD, teamname);
    }
    
    public ManageTeamsPage clickOkButton(){
    	clickOnElement(OK_BUTTON);
    	waitForInvisibility(OK_BUTTON);
        waitForPageToLoad();
    	return new ManageTeamsPage(driver);
    }
}
