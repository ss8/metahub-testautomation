package pages;

import org.openqa.selenium.chrome.ChromeDriver;


public class ManageDataSourcePage extends AbstractManagePage{
			
    final String NEW_DATA_SOURCE = "//span[@class='bp3-button-text' and contains(text(),'New Data Source')]";

    public ManageDataSourcePage(ChromeDriver driver) {
        super(driver);
    }

    public CreateDataSourcePage clickDataSourceUser(){
    	clickOnElement(NEW_DATA_SOURCE);
    	waitForPageToLoad();
    	return new CreateDataSourcePage(driver);
    }
    
}
