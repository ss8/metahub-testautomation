package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateAndEditUserPage extends AbstractPage{
			
    final String USERNAME_TXT_FIELD = "//input[@data-test='input-userID']";
    final String EMAIL_TXT_FIELD = "//input[@data-test='input-email']";
    final String EMAIL_ERROR_LABEL = "//label[@class='bp3-label field-error-message' and contains(.,'Please enter a valid email address.')]";
    final String USERNAME_ERROR_LABEL = "//label[@class='bp3-label field-error-message' and contains(.,'Must be between 1-255 characters')]";
    final String FIRST_NAME_TXT_FIELD = "//input[@data-test='input-firstName']";
    final String LAST_NAME_TXT_FIELD = "//input[@data-test='input-lastName']";
    final String ALL_ROLES_TEXT_FIELD = "//input[@class='bp3-input-ghost bp3-multi-select-tag-input-input']";
    final String ROLE_SELECTOR = "//div[@class='bp3-text-overflow-ellipsis bp3-fill' and contains(text(),'%s')]";
    final String REMOVE_ROLE_BUTTON = "//*[contains(text(),'%s')]/../button";
    final String PASSWORD_TXT_FIELD = "//input[@data-test='input-password']";
    final String CANCEL_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Cancel')]";
    final String SAVE_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Save')]";
  

    public CreateAndEditUserPage(ChromeDriver driver) {
        super(driver);
    }

    public void editUserName(String username){
    	clearTextField(USERNAME_TXT_FIELD);
    	writeOnTextField(USERNAME_TXT_FIELD, username);
    }
    
    public void editUserEmail(String email){
    	clearTextField(EMAIL_TXT_FIELD);
    	writeOnTextField(EMAIL_TXT_FIELD, email);
    }
    
    public void emailErrorVisible(){
    	waitForVisibility(EMAIL_ERROR_LABEL);
    }
    
    public void usernameErrorVisible(){
    	waitForVisibility(USERNAME_ERROR_LABEL);
    }
    
    public void editUserFirstName(String firstname){
    	clearTextField(FIRST_NAME_TXT_FIELD);
    	writeOnTextField(FIRST_NAME_TXT_FIELD, firstname);
    }
    
    public void editUserLastName(String lastname){
    	clearTextField(LAST_NAME_TXT_FIELD);
    	writeOnTextField(LAST_NAME_TXT_FIELD, lastname);
    }
    
    public void addRole(String role){
    	String roleXpath = String.format(ROLE_SELECTOR, role);
    	clickOnElement(ALL_ROLES_TEXT_FIELD);
    	hoverAndClickOnElement(roleXpath);
    	clickOnElement(USERNAME_TXT_FIELD);
    }
    
    public void removeRole(String role){
    	String roleXpath = String.format(REMOVE_ROLE_BUTTON, role);
    	clickOnElement(roleXpath);
    	clickOnElement(LAST_NAME_TXT_FIELD);
    }
    
    public void editUserPassword(String password){
    	clearTextField(PASSWORD_TXT_FIELD);
    	writeOnTextField(PASSWORD_TXT_FIELD, password);
    }
    
    public ManageUsersPage clickCancelButton(){
    	clickOnElement(CANCEL_BUTTON);
    	waitForInvisibility(CANCEL_BUTTON);
    	waitForPageToLoad();
    	return new ManageUsersPage(driver);
    }
    
    public ManageUsersPage clickSaveButton(){
    	clickOnElement(SAVE_BUTTON);
    	waitForInvisibility(SAVE_BUTTON);
    	waitForPageToLoad();
    	return new ManageUsersPage(driver);
    }  
    
}
