package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPage {
	
    protected ChromeDriver driver;

    public AbstractPage (ChromeDriver driver) {
        this.driver = driver;
    }

    public void waitForVisibility(String elementXpath){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
    }

    public void waitForClickable(String elementXpath){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
    }

    public void waitForInvisibility(String elementXpath){
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(elementXpath)));
    }

    public boolean isElementSelected(String elementXpath){
        WebElement webElement = driver.findElement(By.xpath(elementXpath));
        return webElement.getAttribute("selected").equals("true");
    }

    // Some elements get reloaded after being clickable, that is the reason for this try catch block and the double wait
    public void clickOnElement(String elementXpath){
        waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        waitForClickable(elementXpath);
        try {
        	webElement.click();
        } catch (StaleElementReferenceException e) {
            clickOnElement(elementXpath);
        }
    }
    
    public void selectFromDropdown(String elementXpath, String value) {
    	Select dropdown = new Select(driver.findElement(By.xpath(elementXpath)));
    	dropdown.selectByValue(value);
    }
    
    public void deleteTextField(String elementXpath){
    	waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        doubleClickOnElement(elementXpath);
        webElement.sendKeys(Keys.DELETE);
    }
    
    public void clearTextField(String elementXpath){
    	waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        webElement.clear();
    }
    
    public void doubleClickOnElement(String elementXpath){
        waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        Actions actions = new Actions(driver);
        actions.doubleClick(webElement).perform();
    }
    
    public void clickOnElementWithEqualXpaths(String elementXpath, int elementPosition){
        WebElement webElement =  getElementWithEqualXpaths(elementXpath, elementPosition);
        waitForClickable(elementXpath);
        try {
        	webElement.click();
        } catch (StaleElementReferenceException e) {
        	clickOnElementWithEqualXpaths(elementXpath,elementPosition);
        }
    }
    
    public void hoverAndClickOnElement(String elementXpath){
        waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        waitForClickable(elementXpath);
        try {
        	Actions builder = new Actions(driver);
        	builder.moveToElement(webElement).perform();
        	builder.moveToElement(webElement).click().perform();
        } catch (StaleElementReferenceException e) {
        	hoverAndClickOnElement(elementXpath);
        }
    }
    
    public void hoverToElement(String elementXpath){
        waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        Actions builder = new Actions(driver);
        builder.moveToElement(webElement).perform();
    }
    
    public void scrollToElement(String elementXpath){
    	waitForClickable(elementXpath);
    	WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
    }
    
    public void JSEClickOnElement(String elementXpath){
        waitForClickable(elementXpath);
        WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        waitForClickable(elementXpath);
        try {
        	((JavascriptExecutor)driver).executeScript("arguments[0].click();", webElement);
        } catch (StaleElementReferenceException e) {
        	JSEClickOnElement(elementXpath);
        }
    }

    public void clickOnElementLog(String elementXpath){
        waitForClickable(elementXpath);
        WebElement mobileElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        System.out.println("coordinates of clicked object: "+ mobileElement.getLocation());
        mobileElement.click();
    }
    

    public void writeOnTextField(String elementXpath, String message){
    	waitForClickable(elementXpath);
        WebElement mobileElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
        mobileElement.sendKeys(message);
    }
    
    public void writeOnTextFieldElementWithEqualXpaths(String elementXpath, String message, int position){
        WebElement mobileElement =  getElementWithEqualXpaths(elementXpath, position);
        mobileElement.sendKeys(message);
    }
    
    public WebElement getElementWithEqualXpaths(String elementXpath, int elementPosition){
    	waitForClickable(elementXpath);
    	List<WebElement> mobileElements =  (List<WebElement>)  driver.findElements(By.xpath(elementXpath));
        return mobileElements.get(elementPosition);
    }
    
    public void closeHelpTab(){
    	Set<String> tabsSet = driver.getWindowHandles();
    	String[] tabs = tabsSet.stream().toArray(String[]::new);
    	if (tabs.length > 1){
	    	driver.switchTo().window(tabs[1]);
	        driver.close();
	        driver.switchTo().window(tabs[0]);
    	}
    }
    
    public void refreshPage(){
    	driver.navigate().refresh();
    	waitForPageToLoad();
    }
    
    public String getTimeStamp(){
    	return new SimpleDateFormat("HHmmss").format(new Date());
    }
    
    public void waitForPageToLoad(){
    	new WebDriverWait(driver, 30).until(
      	      webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }
    
    public void pressEnterButton(String elementXpath) {
    	waitForClickable(elementXpath);
    	WebElement webElement =  (WebElement)  driver.findElement(By.xpath(elementXpath));
    	webElement.sendKeys(Keys.ENTER);
    }
    
    public void inputFile(String filePath) throws AWTException {
    	StringSelection ss = new StringSelection(filePath);
    	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

        //imitate mouse events like ENTER, CTRL+C, CTRL+V
    	Robot robot = new Robot();
    	robot.delay(250);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	robot.keyPress(KeyEvent.VK_CONTROL);
    	robot.keyPress(KeyEvent.VK_V);
    	robot.keyRelease(KeyEvent.VK_V);
    	robot.keyRelease(KeyEvent.VK_CONTROL);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.delay(50);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    }
      
}
