package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class ManageRolesPage extends AbstractManagePage{
			
    final String NEW_ROLE_BTN = "//button[@class='bp3-button']";
    final String SEARCH_TXT_FIELD = "//input[@class='bp3-input']";
    final String REFRESH_BTN= "//span[@class='bp3-icon bp3-icon-refresh']";
    final String TABLE_ROW_ELEMENT = "//div[contains(text(),'%s')][1]/../div";
    final String VIEW_FIRST_ROLE_BTN = "//*[@id=\"view\"]/div";
    final String EDIT_FIRST_ROLE_BTN = "//*[@id=\"edit\"]/div";
    final String DELETE_FIRST_ROLE_BTN = "//*[@id=\"delete\"]/div";

    public ManageRolesPage(ChromeDriver driver) {
        super(driver);
    }
    
    public CreateAndEditRolePage clickCreateRole(){
    	clickOnElement(NEW_ROLE_BTN);
    	waitForPageToLoad();
    	return new CreateAndEditRolePage(driver);
    }
    
    public void inputSearchRole(String text){
    	deleteSearchRole();
    	writeOnTextField(SEARCH_TXT_FIELD,text);
    }
    
    public void deleteSearchRole(){
    	deleteTextField(SEARCH_TXT_FIELD);
    }
    
    public void clickRefreshButton(){
    	clickOnElement(REFRESH_BTN);
    }
    
    public void clickTableElement(String fieldName, int columnNumber){
    	String elementXpath = String.format(TABLE_ROW_ELEMENT, fieldName);
    	elementXpath = elementXpath + "["+ columnNumber + "]";
    	clickOnElement(elementXpath);
    }
    
    public void clickThreeDotsForRole(String fieldName){
    	String elementXpath = String.format(TABLE_ROW_ELEMENT, fieldName);
    	elementXpath = elementXpath + "["+ 1 + "]";
    	hoverAndClickOnElement(elementXpath);
    }
    
    public ViewRolePage clickViewRole(String roleName){
    	clickThreeDotsForRole(roleName);
    	hoverAndClickOnElement(VIEW_FIRST_ROLE_BTN);
    	waitForPageToLoad();
    	return new ViewRolePage(driver);
    }
    
    public CreateAndEditRolePage clickEditRole(String roleName){
    	clickThreeDotsForRole(roleName);
    	hoverAndClickOnElement(EDIT_FIRST_ROLE_BTN);
    	waitForPageToLoad();
    	return new CreateAndEditRolePage(driver);
    }
    
    public DeletePage clickDeleteRole(String roleName){
    	clickThreeDotsForRole(roleName);
    	hoverAndClickOnElement(DELETE_FIRST_ROLE_BTN);
    	waitForPageToLoad();
    	return new DeletePage(driver);
    }
}
