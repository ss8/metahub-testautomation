package pages;

import org.openqa.selenium.chrome.ChromeDriver;

public class ViewUserPage extends AbstractPage{
			
	final String USERNAME_TXT_FIELD = "//input[@data-test='input-userID']";
    final String EMAIL_TXT_FIELD = "//textarea[@data-test='input-description']";
    final String FIRST_NAME_TXT_FIELD = "//input[@data-test='input-firstName']";
    final String LAST_NAME_TXT_FIELD = "//input[@data-test='input-lastName']";
    final String ALL_ROLES_TEXT_FIELD = "//input[@class='bp3-input-ghost bp3-multi-select-tag-input-input']";
    final String ROLE_SELECTOR = "//div[@class='bp3-text-overflow-ellipsis bp3-fill' and contains(text(),'%s')]";
    final String PASSWORD_TXT_FIELD = "//input[@data-test='input-password']";
    final String OK_BUTTON = "//span[@class='bp3-button-text' and contains(text(),'Ok')]";

    public ViewUserPage(ChromeDriver driver) {
        super(driver);
    }
    
    public void editUserName(String username){
    	writeOnTextField(USERNAME_TXT_FIELD, username);
    }

    public ManageUsersPage clickOkButton(){
    	clickOnElement(OK_BUTTON);
    	waitForInvisibility(OK_BUTTON);
    	return new ManageUsersPage(driver);
    }
}
