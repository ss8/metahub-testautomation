package pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPage extends AbstractPage{                    
	String URL ="";
	final String USERNAME_TXT_FIELD ="//input[@name='login']";
    final String PASSWORD_TXT_FIELD ="//input[@name='password']";
    final String SIGNIN_BUTTON ="//button[@type='submit']";
    final String WRONG_PASSWORD_ERROR = "//span[contains(text(),'Wrong password or username')]";

    public LoginPage (ChromeDriver driver, String start) throws IOException {
        super(driver);
        Properties properties = new Properties();
        String currentPath = new java.io.File(".").getCanonicalPath();
    	FileInputStream fileInputStream = new FileInputStream(currentPath + "\\src\\test\\resources\\properties\\testdata.properties");
    	properties.load(fileInputStream);
    	URL = properties.getProperty("url");
        ((JavascriptExecutor)driver).executeScript("window.location = \'"+URL+"\'");
    }
    
    public LoginPage (ChromeDriver driver) {
        super(driver);
    }
    
    public void wrongPassErrorVisible(){
    	waitForVisibility(WRONG_PASSWORD_ERROR);
    }
    
    public ManageUsersPage clickSubmitButton(){
    	clickOnElement(SIGNIN_BUTTON);
    	return new ManageUsersPage(driver);
    }
    
    public void typeUsername(String username){
    	clearTextField(USERNAME_TXT_FIELD);
        writeOnTextField(USERNAME_TXT_FIELD, username);
    }
    
    public void typePassword(String password){
    	clearTextField(PASSWORD_TXT_FIELD);
        writeOnTextField(PASSWORD_TXT_FIELD, password);
    }

}
