package support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Starter {
    public static ChromeDriver setUp(String Browser) throws IOException{
    	String currentPath = new java.io.File(".").getCanonicalPath();
    	System.setProperty("webdriver.chrome.driver", currentPath + "\\src\\test\\resources\\chromedriver.exe");
    	ChromeDriver driver;
    	ChromeOptions capability = new ChromeOptions();
    	capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
    	capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
    	driver = new ChromeDriver(capability);
    	driver.manage().window().maximize();
    	//Deleting all the cookies
    	driver.manage().deleteAllCookies();
    	//Specifying pageLoadTimeout and Implicit wait
    	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }
}
