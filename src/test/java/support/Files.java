package support;

public class Files {

    public static Image NumberKeyboard(String file) {
        return new Image("NumberKeyboard", file);
    }

    public static Image PillStatesInPacket(String file) {
        return new Image("PillStatesInPacket", file);
    }

}
